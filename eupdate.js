/*
* Copyright(c) 2019 Gabriel Braun
*/
const request = require('request');
const fs = require('fs');
let update_files = [];
let update_files_length = 0;

exports.update = function update() {
  let raw_data = fs.readFileSync('./eupdate/eupdate.json');
  let localdata = JSON.parse(raw_data);


  request({
    url: "http://eupdate.braun-ft.com/eupdate.json",
    method: 'GET',
  }, function(error, response, body){
    let updatedata = JSON.parse(body);
    if(error == null) {

      if(localdata.id < updatedata.id) {
        console.log("###!### Outdated!");
        let files = updatedata.files;
        let id = updatedata.id;

        update_files = files;
        update_files_length = files.length;
        console.log(update_files_length);
        if(update_files_length > 0) {
          requestfile(0, update_files[0], id);
        }

        /*
        for (let file of files) {
          requestfile(file, id);
        }
        */
        conf_data = JSON.stringify(updatedata, null, 2);
        try {
          fs.writeFileSync("./eupdate/eupdate.json", conf_data);
        }
        catch (e) {
          console.log(e);
        }


      }
      else {
        console.log("###!### Up to date!");
      }
    }
    else {
      console.log(error);
    }
  });
}

function requestfile(nr, file, id) {
  if(nr >= update_files_length) {
    return true;
  }
  else {
    request({
      url: "http://eupdate.braun-ft.com/"+id+"/"+file.filepath+file.filename,
      method: 'GET',
    }, function(error, response, body){
      if(error == null) {
        console.log("###!### Updated File: "+file.filepath+file.filename);
        try {
          fs.writeFileSync("./"+file.filepath+file.filename, body);
        }
        catch (e) {
          console.log(e);
        }
        return requestfile(++nr, update_files[nr], id);
      }
      else {
        console.log(error);
      }
    });
  }
}
